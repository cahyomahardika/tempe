package techsoft.com.tekansikun.controllers.product;

import android.os.AsyncTask;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.reactiveandroid.ReActiveAndroid;
import com.reactiveandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.comp.adapter.ProductListAdapter;
import techsoft.com.tekansikun.database.bo.BoProduk;
import techsoft.com.tekansikun.database.model.Produk;
import techsoft.com.tekansikun.system.AppDatabase;
import techsoft.com.tekansikun.system.Connection;
import techsoft.com.tekansikun.system.Response;
import techsoft.com.tekansikun.system.Window;

/**
 * Created by USER on 04/10/2018.
 */

public class Product extends Window {
    private TextView lblTotalPrice;
    private ImageView imgBack;
    private Button btnNext;
    private RecyclerView gridProduct;
    private LinearLayout linearLayout;
    private List<Produk> productList = new ArrayList<>();
    private ProductListAdapter productListAdapter;
    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.product);
        btnNext = getButton(R.id.btnNext);
        imgBack = getImageView(R.id.imgBack);
        lblTotalPrice = getTextView(R.id.lblTotalPrice);
        gridProduct = getRecyclerView(R.id.gridProduct);
        linearLayout = getLinearLayout(R.id.linearLayout);
    }

    @Override
    protected void initEventListener() {
        super.initEventListener();

        btnNext.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                showTooltip("Coming soon. . .");
            }
        }));

        imgBack.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }));
    }

    @Override
    protected void updateComponent() {
        super.updateComponent();
        if(lblTotalPrice.getText().toString().equals(" Rp0,-"))
        {
            linearLayout.setVisibility(View.GONE);
        }
        else
        {
            linearLayout.setVisibility(View.VISIBLE);
        }
        getDataProduk();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private  void setAdapter(){
        productList = Select.from(Produk.class).fetch();
        productListAdapter = new ProductListAdapter(productList, this);
        gridProduct.setLayoutManager(new GridLayoutManager(this, 1));
        gridProduct.setAdapter(productListAdapter);
    }

    public void setTotalPrice(String totalPrice){
        lblTotalPrice.setText(totalPrice);
        if(lblTotalPrice.getText().toString().equals(" Rp0,-")||lblTotalPrice.getText().toString().equals("Rp0,-"))
        {
            linearLayout.setVisibility(View.GONE);
        }
        else
        {
            linearLayout.setVisibility(View.VISIBLE);
        }
    }


    private void getDataProduk() {

        String url = API_URL+"product/read.php";

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String data) {
                        getDataProdukResponse(Connection.getResponseFromJSON(data));
                        Log.e("Cek Produk", data);
                    }
                }, errorListener);

        request.setRetryPolicy(new DefaultRetryPolicy(
                Connection.MY_SOCKET_TIMEOUT_MS,
                Connection.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(getContext()).add(request);

    }

    private void getDataProdukResponse(final Response response) {
        if (response.isValid()) {
            AsyncTask async = new AsyncTask() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Object doInBackground(Object[] params) {
                    try {
                        JSONObject object = response.getObject();

                        ReActiveAndroid.getDatabase(AppDatabase.class).beginTransaction();
                        try {
                            if(!object.isNull("products")){
                                JSONArray JData = object.getJSONArray("products");
                                BoProduk boProduk = new BoProduk();
                                boProduk.doSave(JData);
                            }

                            ReActiveAndroid.getDatabase(AppDatabase.class).getWritableDatabase().setTransactionSuccessful();

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            ReActiveAndroid.getDatabase(AppDatabase.class).endTransaction();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
//                    showTooltip(response.getMessage());
                    setAdapter();
                }
            };

            async.execute();
        }else{

            showTooltip(response.getMessage());

        }
    }

    private com.android.volley.Response.ErrorListener errorListener = new com.android.volley.Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
            showTooltip("Terjadi kesalahan saat koneksi ke server");
        }
    };
}
