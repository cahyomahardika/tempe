package techsoft.com.tekansikun.controllers.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Bayu on 11/08/2016.
 */
public class MyGcmListenerAutorun extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent service = new Intent(context, MyGcmListenerService.class);
            context.startService(service);
        }
        catch (IllegalStateException e)
        {
            Log.e("Error startService",e.toString());
        }
    }
}
