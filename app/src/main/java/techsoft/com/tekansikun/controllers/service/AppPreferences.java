package techsoft.com.tekansikun.controllers.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Bayu on 18/07/2016.
 */
public class AppPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final int MY_SOCKET_TIMEOUT_MS = 10000;
    public static final int DEFAULT_MAX_RETRIES = 3;

    public static String getGcmId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE);
        return preferences.getString("gcm_id", "");
    }

    public static void saveGcmId(Context context, String gcmId) {
        SharedPreferences preferences = context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE);
        preferences.edit().putString("gcm_id", gcmId).commit();
    }


    public static String getCredential(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE);
        return preferences.getString("username", "");
    }

    public static void saveCredential(Context context, String username) {
        SharedPreferences preferences = context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE);
        preferences.edit().putString("username", username).commit();
    }

    public static Boolean hasGcmId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(AppPreferences.SENT_TOKEN_TO_SERVER, false);
    }

    public static void removeGcmId(Context context) {
        saveGcmId(context, "");
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(AppPreferences.SENT_TOKEN_TO_SERVER, false).commit();
    }

    public static void setLoggedIn(Context context, boolean isLoggedIn) {
        context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE).edit()
                .putBoolean("is_logged_in", isLoggedIn).commit();
    }

    public static Boolean isLoggedIn(Context context) {
        return context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE)
                .getBoolean("is_logged_in", false);
    }
}
