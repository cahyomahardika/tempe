package techsoft.com.tekansikun.controllers.sign;

import android.widget.Button;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.controllers.dashboard.Home;
import techsoft.com.tekansikun.system.Window;

/**
 * Created by USER on 13/12/2018.
 */

public class SignUp extends Window {
    private Button btnDaftar;

    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.signup);
        btnDaftar = getButton(R.id.btnDaftar);
    }

    @Override
    protected void initEventListener() {
        super.initEventListener();
        btnDaftar.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(Home.class);
            }
        }));
    }

    @Override
    protected void updateComponent() {
        super.updateComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
