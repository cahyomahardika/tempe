package techsoft.com.tekansikun.controllers.tentangkami;

import androidx.cardview.widget.CardView;
import android.widget.ImageView;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.system.Window;

/**
 * Created by USER on 22/10/2018.
 */

public class TentangKami extends Window {

    private CardView cardBiodata, cardJenisProduk, cardSejarah, cardCaraPembuatan, cardPenghargaan;

    private ImageView imgBack;
    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.tentang_kami);

        cardBiodata = (CardView) findViewById(R.id.cardBiodata);
        cardCaraPembuatan = (CardView) findViewById(R.id.cardCaraPembuatan);
        cardJenisProduk = (CardView) findViewById(R.id.cardJenisProduk);
        cardSejarah = (CardView) findViewById(R.id.cardSejarah);
        cardPenghargaan = (CardView) findViewById(R.id.cardPenghargaan);
        imgBack = getImageView(R.id.imgBack);

    }

    @Override
    protected void initEventListener() {
        super.initEventListener();

        cardBiodata.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
            showTooltip("Biodata");
            }
        }));

        cardJenisProduk.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                showTooltip("Jenis Produk");
            }
        }));

        cardSejarah.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                showTooltip("Sejarah");
            }
        }));

        cardCaraPembuatan.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                showTooltip("Cara Pembuatan");
            }
        }));

        cardPenghargaan.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                showTooltip("Penghargaan");
            }
        }));

        imgBack.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }));
    }

    @Override
    protected void updateComponent() {
        super.updateComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
