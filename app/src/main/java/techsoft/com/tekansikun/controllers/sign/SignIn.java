package techsoft.com.tekansikun.controllers.sign;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.controllers.dashboard.Home;
import techsoft.com.tekansikun.system.Window;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by USER on 13/12/2018.
 */

public class SignIn extends Window {
    private Button btnLogin;
    private TextView lblDaftar;
    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private LoginButton loginButton;

    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.signin);
        btnLogin = getButton(R.id.btnLogin);
        lblDaftar = getTextView(R.id.lblDaftar);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));

    }

    @Override
    protected void initEventListener() {
        super.initEventListener();
        btnLogin.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(Home.class);
            }
        }));
        lblDaftar.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(SignUp.class);
            }
        }));
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };
        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

            }
        };

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                GraphRequest request = GraphRequest.newMeRequest(accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("aa", ""+response.toString());
                                try {
//                                    showTooltip("Hi, " + object.getString("name"));
                                    Log.e("bb", "Nama : "+object.getString("name")+" Email : "+object.getString("email"));
                                    controller(Home.class);
//                                    Toast.makeText(getApplicationContext(), "Hi, " + object.getString("name"), Toast.LENGTH_LONG).show();
                                } catch(JSONException ex) {
                                    Log.e("ex", ""+ex.toString());
                                    ex.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,location");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                showTooltip(exception.toString());
            }
        });

    }

    @Override
    protected void updateComponent() {
        super.updateComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }
}
