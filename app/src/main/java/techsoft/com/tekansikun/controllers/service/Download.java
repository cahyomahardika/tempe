package techsoft.com.tekansikun.controllers.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import techsoft.com.tekansikun.system.Visione;


/**
 * Created by Bayu on 13/07/2016.
 */
public class Download extends IntentService {
    private int result = Activity.RESULT_CANCELED;
    private Visione visi = new Visione();

    public static final String URL = "urlpath";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = "filepath";
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "download_service";
    public static final String CODE = "code";
    public static final String PROGRESS = "progress";
    public static final String ISPROGRESS = "isprogress";

    private String code = "";

    public Download() {
        super("Download");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        doDownload(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    private void doDownload(final Intent intent) {

        AsyncTask asyncTask = new AsyncTask() {

            File output;

            @Override
            protected Object doInBackground(Object[] params) {
                String urlPath = intent.getStringExtra(URL);
                String filePath = intent.getStringExtra(FILEPATH);
                String fileName = intent.getStringExtra(FILENAME);
                Boolean isProgress = intent.getStringExtra(ISPROGRESS) == null ? true : intent.getStringExtra(ISPROGRESS).equals("false") ? false : true;

                InputStream stream = null;
                FileOutputStream fos = null;
                output = new File(filePath, fileName);

                try {
                    code = intent.getStringExtra(CODE);

                    URL url = new URL(urlPath);
                    stream = new BufferedInputStream(url.openStream());
                    fos = new FileOutputStream(output.getPath());

                    byte data[] = new byte[1024];
                    int next = -1;
                    int sizeNow = 0;
                    int sizeTotal = url.openConnection().getContentLength();
                    while ((next = stream.read(data)) != -1) {
                        fos.write(data, 0, next);

                        if (isProgress) {
                            sizeNow += next;
                            int percent = (int) ((sizeNow * 100) / sizeTotal);
                            doSendBroadcast(output.getAbsolutePath(), 2, percent);
                        } else {
                            sizeNow += next;
                            int percent = (int) ((sizeNow * 100) / sizeTotal);
                        }
                    }
                    fos.flush();
                    result = Activity.RESULT_OK;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (stream != null) {
                        try {
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (output != null) {
                    doSendBroadcast(output.getAbsolutePath(), result, 100);
                } else {
                    doSendBroadcast("", result, 100);
                }
            }
        };
        asyncTask.execute();
    }

    private void doSendBroadcast(String outputPath, int result, int progress) {
        Intent intent = new Intent(NOTIFICATION + code);
        intent.putExtra(FILEPATH, outputPath);
        intent.putExtra(RESULT, result);
        intent.putExtra(PROGRESS, progress);
        sendBroadcast(intent);
    }
}
