package techsoft.com.tekansikun.controllers.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.List;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.system.Lang;
import techsoft.com.tekansikun.system.Parameter;
import techsoft.com.tekansikun.system.Visione;


public class MyGcmListenerService extends GcmListenerService implements Lang {
    private long[] vibrateAttr = {0, 1000, 200 };
    private String ring_notification = "android.resource://visione.com.oen/" + R.raw.ring_notification;
    private int density;
    private int size = 64;
    private Visione visi = new Visione();
    private LocalBroadcastManager broadcastManager;
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcastManager = LocalBroadcastManager.getInstance(this);
        visi.setContext(this);

    }

    @RequiresApi(api = 26)
    @Override
    public void onMessageReceived(String from, Bundle data) {
            try
            {
                int currentVersion = Build.VERSION.SDK_INT;
                int honeycombVersion = Build.VERSION_CODES.HONEYCOMB;
                int oreoVersion = Build.VERSION_CODES.O;

                if(from.equals(Lang.API_GCM_SENDER)) {
                    visi.log("Incoming GCM "+data.toString());
                    String topic = data.getString("topic");
                    if (topic != null) {
                        if(topic.equals("test"))
                        {
                            if(data.get ("message")!= null)
                            {
                                String message = data.getString("message");
                                if(currentVersion>=oreoVersion)
                                {

                                }
                                else
                                {
                                    notification(0,"Test",message,null, R.drawable.logo,null);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                Log.e("Error GCM",e.toString());
            }
        }

    /*
    *       ----------------------------------------------------------------------------------------
    *       PRIVATE FUNCTION
    *       ----------------------------------------------------------------------------------------
    */



    /*
    *       ----------------------------------------------------------------------------------------
    *       PRIVATE FUNCTION
    *       ----------------------------------------------------------------------------------------
    */

    @RequiresApi(api = 26)
    private void notification(final int index, final String title, final String message, final Class controller, final Integer icon, final List<Parameter> parameters) {
        int currentVersion = Build.VERSION.SDK_INT;
        int honeycombVersion = Build.VERSION_CODES.HONEYCOMB;
        int oreoVersion = Build.VERSION_CODES.O;
        Notification notif;
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (controller != null) {
            if (currentVersion >= oreoVersion) {
                createNotifChannel(this,title,message, controller, icon);
            }
            if (currentVersion >= honeycombVersion) {
                Intent intent = new Intent(this, controller);

                if (parameters != null && parameters.size() > 0) {
                    Bundle bundle = new Bundle();
                    for (Parameter parameter : parameters) {
                        bundle.putString(parameter.getName(), parameter.getValue());
                    }

                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }

                PendingIntent pIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, controller), 0);

                notif = new Notification.Builder(this).setLights(Color.BLUE, 500, 500)
                        .setContentTitle(title).setContentText(message)
                        .setSmallIcon(icon).setContentIntent(pIntent)
                        .getNotification();

            } else {
                notif = new Notification(icon, message, 10000);
            }
        } else {
            if (currentVersion >= honeycombVersion) {
                PendingIntent pIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, controller), PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                notif = new NotificationCompat.Builder(this).setLights(Color.BLUE, 500, 500)
                        .setContentTitle(title).setContentText(message).setContentIntent(pIntent)
                        .setSmallIcon(icon).getNotification();

            } else {
                notif = new Notification(icon, message, 10000);
            }
        }

        notif.flags = NotificationCompat.FLAG_AUTO_CANCEL;
        notif.vibrate = vibrateAttr;
        notif.sound = Uri.parse(ring_notification);
        notificationManager.notify(index, notif);
        stopSelf();
    }

    public static final String NOTIF_CHANNEL_ID = "my_channel_01";

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotifChannel(Context context,String title, String message,final Class controller, final Integer icon) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        NotificationChannel mChannel = new NotificationChannel(NOTIF_CHANNEL_ID,"channel-01",  NotificationManager.IMPORTANCE_HIGH);

        Intent notificationIntent = new Intent(context, controller);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, controller), 0);

        Notification notification =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentIntent(pIntent)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setChannelId(NOTIF_CHANNEL_ID).build();

        notification.flags = NotificationCompat.FLAG_AUTO_CANCEL;

        mChannel.enableVibration(true);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), Uri.parse(ring_notification));
        r.play();
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(mChannel);
        mNotificationManager.notify(1 , notification);
    }

    private boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }

        return true;
    }

    private com.android.volley.Response.ErrorListener errorListener = new com.android.volley.Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
        }
    };

    private String encode(String string) {
        byte[] bytesEncoded = Base64.encode(new String(string).getBytes(), 1000);
        return new String(bytesEncoded).trim();
    }

    public void controllerClearHistory(Class clas) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void openAPKPlayStore(){
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "visione.com.oen"));
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET|Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(marketIntent);
    }

    protected String version_apk(){
        String versionName = "";
        int versionCode = 0;
        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            //versionName = pinfo.versionName;
            versionCode = pinfo.versionCode;

            versionName = String.valueOf(versionCode);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

}