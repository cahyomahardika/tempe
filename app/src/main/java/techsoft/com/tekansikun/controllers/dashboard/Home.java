package techsoft.com.tekansikun.controllers.dashboard;

import androidx.viewpager.widget.ViewPager;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.comp.adapter.ImagePagerAdapter;
import techsoft.com.tekansikun.controllers.gallery.Gallery;
import techsoft.com.tekansikun.controllers.product.Product;
import techsoft.com.tekansikun.controllers.tentangkami.TentangKami;
import techsoft.com.tekansikun.system.Window;

/**
 * Created by USER on 19/10/2018.
 */

public class Home  extends Window{

    private Timer timer;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private ImagePagerAdapter imagePagerAdapter;
    List<String> imageUrlList = new ArrayList<>();
    private Button btnToProduct,btnToGallery,btnToTentangKami;
    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.home2);

        viewPager = getPagerView(R.id.viewpager);
        indicator = getCircleIndicator(R.id.indicator);
        btnToGallery = getButton(R.id.btnToGallery);
        btnToProduct = getButton(R.id.btnToProduct);
        btnToTentangKami = getButton(R.id.btnToTentangKami);
        timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
    }

    @Override
    protected void initEventListener() {
        super.initEventListener();
        btnToProduct.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(Product.class);
            }
        }));
        btnToGallery.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(Gallery.class);
            }
        }));
        btnToTentangKami.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                controller(TentangKami.class);
            }
        }));
    }

    @Override
    protected void updateComponent() {
        super.updateComponent();
        setSlider();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    private void setSlider()
    {

        imageUrlList.clear();
        imageUrlList.add("https://cdn0-production-images-kly.akamaized.net/PN6sXBZafX_-ztb9hMW4NVsm9ek=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2347568/original/070938000_1535775186-30htempe1-5b888dbfe9fbc.jpg");
        imageUrlList.add("http://universitaspahlawan.ac.id/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-28-at-13.34.50.jpeg");
        imageUrlList.add("http://2.bp.blogspot.com/-epw63N3-wN0/VZAj9y9ILPI/AAAAAAAAAHM/CtF89iZe69c/s1600/FB_IMG_1435387550274.jpg");

        imagePagerAdapter = new ImagePagerAdapter(imageUrlList);
        viewPager.setAdapter(imagePagerAdapter);
        indicator.setViewPager(viewPager);

    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            Home.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < imageUrlList.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
