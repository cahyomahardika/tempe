package techsoft.com.tekansikun.comp.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import techsoft.com.tekansikun.R;
import techsoft.com.tekansikun.controllers.product.Product;
import techsoft.com.tekansikun.database.model.Produk;
import techsoft.com.tekansikun.system.Visione;

import static techsoft.com.tekansikun.system.Visione.onClick;


/**
 * Created by USER on 07/08/2018.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductHolder> {

    Context context;
    private List<Produk> objects;
    private Visione visione = new Visione();

    double totalPrice = 0;
    public ProductListAdapter(List<Produk> products, Context context) {
        this.objects = products;
        this.context = context;
//        visione.setContext(context);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        ProductHolder holder = new ProductHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {
        Produk object = objects.get(position);
        holder.lblProductName.setText(object.getNamaProduk());
        holder.lblProductPrice.setText("Rp"+getMoneyFormat(Integer.parseInt(object.getHargaProduk()))+",-");

        Glide.with(context)
                .load(objects.get(position).getFotoProduk())
                .error(R.drawable.logo)
                .placeholder(R.drawable.logo)
                .dontAnimate()
                .into(holder.imgProduct);

        initEventListener(object, holder);

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    private void initEventListener(final Produk object, final ProductListAdapter.ProductHolder holder) {
//        holder.imgProductMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Session.product = object;
//
//                FragmentManager fm = ((FragmentActivity)context).getSupportFragmentManager();
//                CtrlProductMenuDialog ctrlProductMenuDialog = CtrlProductMenuDialog.newInstance(object.getName());
//                ctrlProductMenuDialog.show(fm, "product_menu_dialog");
//            }
//        });
//
//        holder.linear_product.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                visione.controller(CtrlProductDetail.class);
//            }
//        });

        holder.btnBeli.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                holder.btnBeli.setVisibility(View.GONE);
                holder.linearBeli.setVisibility(View.VISIBLE);

                totalPrice = totalPrice +  Integer.parseInt(holder.lblQty.getText().toString())*Integer.parseInt(object.getHargaProduk());
                ((Product)context).setTotalPrice("Rp"+getMoneyFormat(totalPrice)+",-");
            }
        }));
        holder.btnPlus.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                int now = Integer.parseInt(holder.lblQty.getText().toString());

                holder.lblQty.setText(now + 1 + "");
                totalPrice = totalPrice +  Integer.parseInt(object.getHargaProduk());
                ((Product)context).setTotalPrice("Rp"+getMoneyFormat(totalPrice)+",-");

            }
        }));
        holder.btnMinus.setOnClickListener(onClick(new Runnable() {
            @Override
            public void run() {
                int now = Integer.parseInt(holder.lblQty.getText().toString());

                if (now > 1) {
                    holder.lblQty.setText(now - 1 + "");
                    totalPrice = totalPrice - Integer.parseInt(object.getHargaProduk());
                    ((Product)context).setTotalPrice("Rp"+getMoneyFormat(totalPrice)+",-");
                } else {
                    totalPrice = totalPrice - Integer.parseInt(object.getHargaProduk());
                    ((Product)context).setTotalPrice("Rp"+getMoneyFormat(totalPrice)+",-");
                    holder.btnBeli.setVisibility(View.VISIBLE);
                    holder.linearBeli.setVisibility(View.GONE);
                }

            }
        }));
    }

    private String getMoneyFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);

        if (hasil.indexOf(",") == -1) {

        }

        return hasil;
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct, imgProductMenu;
        TextView lblProductName, lblQty, lblProductPrice;
        Button btnBeli, btnPlus, btnMinus;
        LinearLayout linearBeli;

        public ProductHolder(View view) {
            super(view);
            imgProduct = view.findViewById(R.id.imgProduct);
            lblProductName = view.findViewById(R.id.lblProductName);
            lblProductPrice = view.findViewById(R.id.lblPrice);
            lblQty = view.findViewById(R.id.lblQty);
            btnBeli = view.findViewById(R.id.btnBeli);
            btnPlus = view.findViewById(R.id.btnPlus);
            btnMinus = view.findViewById(R.id.btnMinus);
            linearBeli = view.findViewById(R.id.linearBeli);
        }
    }
}
