package techsoft.com.tekansikun.comp.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by asus on 27/10/2016.
 */
public class FontRegulerButton extends Button {


    public FontRegulerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf"));
    }


    public FontRegulerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}