package techsoft.com.tekansikun.comp.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by asus on 27/10/2016.
 */
public class FontLightButton extends Button {


    public FontLightButton(Context context, AttributeSet attrs) {
        super(context, attrs);

            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Roboto-Light.ttf"));

    }


    public FontLightButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}