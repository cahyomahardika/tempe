package techsoft.com.tekansikun.comp.adapter;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import techsoft.com.tekansikun.R;


/**
 * Created by USER on 09/08/2018.
 */

public class ImagePagerAdapter extends PagerAdapter {

    List<String> imageUrl;

    public ImagePagerAdapter(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }

    @Override
    public int getCount() {
        return imageUrl.size();
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View imageLayout = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_slider_item, container, false);
        ImageView imageView = (ImageView) imageLayout.findViewById(R.id.img);
        Glide.with(container.getContext()).load(imageUrl.get(position)).asBitmap().into(imageView);
        container.addView(imageLayout, 0);
        return imageLayout;

    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view == ((ImageView) object);
    }
}
