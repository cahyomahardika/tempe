package techsoft.com.tekansikun.system.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.reactiveandroid.Model;
import com.reactiveandroid.query.Delete;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import techsoft.com.tekansikun.system.Lang;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String formateDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
        String newFormat = formatter.format(date);
        return newFormat;
    }



    public static Bitmap getBitmapFromPath(String s, int width, int height) {
        File sd = Environment.getExternalStorageDirectory();
        File image = new File(s);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        if (width != 0 && height != 0) {
            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        }
        return bitmap;
    }



    public static void copyDirectory(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists() && !targetLocation.mkdirs()) {
                throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            // make sure the directory we plan to store the recording in exists
            File directory = targetLocation.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    public static void logw(String tag, String message) {
        Log.w(tag, " " + message);
    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }


    public static String getMoneyFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);

        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
        }
//        return "Rp. " + hasil;
        return hasil;
    }


    public static String stringToUrl(String text) {
        String urlValidationRegex = "(https?|ftp)://(www\\d?|[a-zA-Z0-9]+)?.[a-zA-Z0-9-]+(\\:|.)([a-zA-Z0-9.]+|(\\d+)?)([/?:].*)?";
        Pattern p = Pattern.compile(urlValidationRegex);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String found = m.group(0);
            m.appendReplacement(sb, "<a href='" + found + "' target='_blank'>"
                    + found + "</a>");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /*
   * ------------------------------------------------------------- Menghitung
   * harga-harga product
   * -------------------------------------------------------------
   */
//    public static List<Double> getProductPrices(ProductList product) {
//        Double price = product.getPrice1();
//        List<Double> prices = new ArrayList<>();
//        prices.add(price);
//
//        if (product.getShop().isJoined() == 1) {
//            switch (product.getShop().getLevelPrice()) {
//                case 1:
//                    if (product.getPrice1() > 0
//                            && product.getShop().isLevel1Active()) {
//                        price = product.getPrice1();
//                    }
//                    break;
//
//                case 2:
//                    if (product.getPrice2() > 0
//                            && product.getShop().isLevel2Active()) {
//                        price = product.getPrice2();
//                        prices.add(price);
//                    }
//                    break;
//
//                case 3:
//                    if (product.getPrice3() > 0
//                            && product.getShop().isLevel3Active()) {
//                        price = product.getPrice3();
//                        prices.add(price);
//                    }
//                    break;
//
//                case 4:
//                    if (product.getPrice4() > 0
//                            && product.getShop().isLevel4Active()) {
//                        price = product.getPrice4();
//                        prices.add(price);
//                    }
//                    break;
//
//                case 5:
//                    if (product.getPrice5() > 0
//                            && product.getShop().isLevel5Active()) {
//                        price = product.getPrice5();
//                        prices.add(price);
//                    }
//                    break;
//
//                default:
//                    price = product.getPrice1();
//                    break;
//            }
//        }
//
//        return prices;
//    }
//
//
//    /*
//    * ------------------------------------------------------------- Menghitung
//    * harga-harga product varian
//    * -------------------------------------------------------------
//    */
//    public static List<Double> getProductVarianPrices(ProductVarian productVarian) {
//        Double priceVarian = productVarian.getPriceVarian1();
//        List<Double> varianPrices = new ArrayList<>();
//        varianPrices.add(priceVarian);
//
//        if (productVarian.getProduct().getShop().isJoined() == 1) {
//            switch (productVarian.getProduct().getShop().getLevelPrice()) {
//                case 1:
//                    if (productVarian.getPriceVarian1() > 0
//                            && productVarian.getProduct().getShop().isLevel1Active()) {
//                        priceVarian = productVarian.getPriceVarian1();
//                    }
//                    break;
//
//                case 2:
//                    if (productVarian.getPriceVarian2() > 0
//                            && productVarian.getProduct().getShop().isLevel2Active()) {
//                        priceVarian = productVarian.getPriceVarian2();
//                        varianPrices.add(priceVarian);
//                    }
//                    break;
//
//                case 3:
//                    if (productVarian.getPriceVarian3() > 0
//                            && productVarian.getProduct().getShop().isLevel3Active()) {
//                        priceVarian = productVarian.getPriceVarian3();
//                        varianPrices.add(priceVarian);
//                    }
//                    break;
//
//                case 4:
//                    if (productVarian.getPriceVarian4() > 0
//                            && productVarian.getProduct().getShop().isLevel4Active()) {
//                        priceVarian = productVarian.getPriceVarian4();
//                        varianPrices.add(priceVarian);
//                    }
//                    break;
//
//                case 5:
//                    if (productVarian.getPriceVarian5() > 0
//                            && productVarian.getProduct().getShop().isLevel5Active()) {
//                        priceVarian = productVarian.getPriceVarian5();
//                        varianPrices.add(priceVarian);
//                    }
//                    break;
//
//                default:
//                    priceVarian = productVarian.getProduct().getPrice1();
//                    break;
//            }
//        }
//
//        return varianPrices;
//    }

    public static String encode(String string) {
        byte[] bytesEncoded = Base64
                .encode(new String(string).getBytes(), 1000);
        return new String(bytesEncoded).trim();
    }

    public static String decoder(String string) {
        byte[] valueDecoded = Base64
                .decode(new String(string).getBytes(), 1000);
        return new String(valueDecoded).trim();
    }

    public static Bitmap getBitmapFromImageView(ImageView imageView) {
        Bitmap bitmap = null;
        Drawable icon = imageView.getDrawable();
        if (icon != null) {
            if (icon instanceof BitmapDrawable) {
                bitmap = ((BitmapDrawable) icon).getBitmap();
            } else {
                bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                icon.draw(canvas);
            }
        }
        return bitmap;
    }

    public static void logFile(String message){
        try{

            File file = new File(Lang.PATH_LOG, "log.txt");
            FileOutputStream fOut = new FileOutputStream(file);
            fOut.write(message.getBytes());
            fOut.flush();
            fOut.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

//    public static User getUser() {
//        return new Select().from(User.class).executeSingle();
//    }
//    public static Shop getShop() {
//        return new Select().from(Shop.class).executeSingle();
//    }
    public static void deleteAll(Class<? extends Model> table) {
        Delete.from(table).execute();
    }
}