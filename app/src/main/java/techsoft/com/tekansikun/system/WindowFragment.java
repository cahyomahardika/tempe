package techsoft.com.tekansikun.system;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.reactiveandroid.Model;
import com.reactiveandroid.query.Delete;
import com.reactiveandroid.query.Select;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint({"SetJavaScriptEnabled", "SimpleDateFormat"})
@SuppressWarnings("rawtypes")
public abstract class WindowFragment extends Fragment implements Lang {
    private Visione visione = new Visione();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        visione.setContext(getContext());
        init();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        updateComponent();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateComponent();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        initPrivateObject();
        initComponent();
        initEventListener();
    }

    protected void initComponent() {
    }

    protected void initPrivateObject() {
    }

    protected void initEventListener() {
    }

    protected void updateComponent() {
    }

    /*
     * ------------------------------- CODE
     * -----------------------------------------
     */

    /* delete semua data di tabel */


    protected boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }

//    protected Device getDevice() {
//        return new Select().from(Device.class).executeSingle();
//    }
//
//    protected User getUser() {
//        return new Select().from(User.class).executeSingle();
//    }

    protected int sizeTableContent(Class<? extends Model> table) {
        return Select.from(table).fetch().size();
    }

    protected void deleteAll(Class<? extends Model> table) {
        Delete.from(table).execute();
    }

    protected void showTooltip(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void controller(Class clas) {
        Intent intent = new Intent(this.getActivity(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void controller(Class clas, List<Parameter> parameters) {
        Intent intent = new Intent(this.getActivity().getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter);
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        startActivity(intent);
    }

    public void controller(Class clas, Parameter... parameters) {
        Intent intent = new Intent(this.getActivity().getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter);
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        startActivity(intent);
    }

    public void controllerNoAction(Class clas) {
        Intent intent = new Intent(this.getActivity().getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void controller(Fragment fragment, Integer containerID) {
        try {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager
                    .beginTransaction().replace(containerID, fragment,
                            "previous");
            if (!fragment.getClass().getSimpleName().equals("CtrlMenu")) {
                transaction.addToBackStack("previous");
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // No call for super(). Bug on API Level > 11.
    }

    protected boolean isValidEmailAddress(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    protected void showInformation(String message) {
        showConfirm(getContext(), "Informasi", message, "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
    }

    protected void showInformation(Context context, String judul, String pesan,
                                   String buttonLabel) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setNeutralButton(buttonLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alert.show();
    }

    protected void showConfirm(Context context, String judul, String pesan,
                               String labelYes, DialogInterface.OnClickListener clickListenerYes) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setPositiveButton(labelYes, clickListenerYes);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    protected void showConfirm(Context context, String judul, String pesan,
                               String labelYes, String labelNo,
                               DialogInterface.OnClickListener clickListenerYes,
                               DialogInterface.OnClickListener clickListenerNo) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setPositiveButton(labelYes, clickListenerYes);
        alert.setNegativeButton(labelNo, clickListenerNo);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    public Object getParameter(String string) {
        return this.getActivity().getIntent().getExtras().get(string);
    }

    public Button getButton(int id) {
        return (Button) this.getActivity().findViewById(id);
    }

    public CheckBox getCheckBox(int id) {
        return (CheckBox) this.getActivity().findViewById(id);
    }

    public TextView getTextView(int id) {
        return (TextView) this.getActivity().findViewById(id);
    }


    public EditText getEditText(int id) {
        return (EditText) this.getActivity().findViewById(id);
    }

    public ImageView getImageView(int id) {
        return (ImageView) this.getActivity().findViewById(id);
    }

    public ScrollView getScrollView(int id) {
        return (ScrollView) this.getActivity().findViewById(id);
    }

    public ProgressBar getProgressBar(int id) {
        return (ProgressBar) this.getActivity().findViewById(id);
    }

    public RadioButton getRadioButton(int id) {
        return (RadioButton) this.getActivity().findViewById(id);
    }

    public RadioGroup getRadioGroup(int id) {
        return (RadioGroup) this.getActivity().findViewById(id);
    }

    public LinearLayout getLinearLayout(int id) {
        return (LinearLayout) this.getActivity().findViewById(id);
    }

    public AutoCompleteTextView getaAutoCompleteTextView(int id){
        return (AutoCompleteTextView) this.getActivity().findViewById(id);
    }

    public RelativeLayout getRelativeLayout(int id) {
        return (RelativeLayout) this.getActivity().findViewById(id);
    }

    public TableLayout getTableLayout(int id) {
        return (TableLayout) this.getActivity().findViewById(id);
    }

    public Spinner getSpinner(int id) {
        return (Spinner) this.getActivity().findViewById(id);
    }

    public ListView getListView (int id){
        return  (ListView) this.getActivity().findViewById(id);
    }




    public WebView getWebView(int id) {
        return (WebView) this.getActivity().findViewById(id);
    }

    public GridView getGridView(int id) {
        return (GridView) this.getActivity().findViewById(id);
    }

    public RecyclerView getRecyclerView(int id){
        return (RecyclerView) this.getActivity().findViewById(id);
    }

    public ViewPager getPagerView(int id) {
        return (ViewPager) this.getActivity().findViewById(id);
    }

    public FrameLayout getFrameLayout(int id) {
        return (FrameLayout) this.getActivity().findViewById(id);
    }

    public Context getContext() {
        if (getActivity() != null) {
            return getActivity();
        } else {
            return AppController.getAppContext();
        }
    }

    @SuppressWarnings("static-access")
    public int color(String color) {
        return new Color().parseColor(color);
    }

    public String encode(String string) {
        byte[] bytesEncoded = Base64.encode(new String(string).getBytes(),
                Base64.DEFAULT);
        return new String(bytesEncoded);
    }

    public String decoder(String string) {
        byte[] valueDecoded = Base64.decode(new String(string).getBytes(),
                Base64.DEFAULT);
        return new String(valueDecoded);
    }

    public void log(String string) {
        Log.w("Bonoboapp.com "
                + new SimpleDateFormat("hh:mm:sss").format(new Date()), string);
    }

    public ArrayAdapter<Object> addSpinner(List<Object> datas) {
        ArrayAdapter<Object> dataAdapter = new ArrayAdapter<Object>(
                this.getActivity(),
                android.R.layout.simple_spinner_dropdown_item, datas);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return dataAdapter;
    }

    public void viewWebsite(WebView browser, String url) {
        browser.setWebViewClient(new WebViewClient());
        browser.setWebChromeClient(new WebChromeClient());
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setBuiltInZoomControls(true);
        browser.loadUrl(url);
    }

    public Bitmap getImage(String url) {
        try {
            URL urlImage = new URL(url);

            HttpURLConnection connection = (HttpURLConnection) urlImage
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();

            InputStream stream = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int getDate4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tanggal = cal.get(Calendar.DATE);
        return tanggal;
    }

    private int getMonth4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int bulan = cal.get(Calendar.MONTH);
        return bulan;
    }

    private int getYear4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tahun = cal.get(Calendar.YEAR);
        return tahun;
    }

    public String getDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd m yyyy");
        return df.format(date);
    }

    public String getDateString(Date date) {
        return getDay4String(date) + ", " + getDate4Integer(date) + " "
                + getMonth4String(date) + " " + getYear4Integer(date);
    }

    public String getDateNoDayString(Date date) {
        return getDate4Integer(date) + " "
                + getMonth4String(date) + " " + getYear4Integer(date);
    }

    public String getDateDatabase(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public String getDay4String(Date date) {
        String day = "Senin";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayInt = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayInt) {
            case 1:
                day = "Minggu";
                break;
            case 2:
                day = "Senin";
                break;
            case 3:
                day = "Selasa";
                break;
            case 4:
                day = "Rabu";
                break;
            case 5:
                day = "Kamis";
                break;
            case 6:
                day = "Jumat";
                break;
            case 7:
                day = "Sabtu";
                break;
        }

        return day;
    }

    public String getMonth4String(Date date) {
        String monthString = "";
        switch (getMonth4Integer(date)) {
            case 0:
                monthString = "Januari";
                break;
            case 1:
                monthString = "Februari";
                break;
            case 2:
                monthString = "Maret";
                break;
            case 3:
                monthString = "April";
                break;
            case 4:
                monthString = "Mei";
                break;
            case 5:
                monthString = "Juni";
                break;
            case 6:
                monthString = "Juli";
                break;
            case 7:
                monthString = "Augustus";
                break;
            case 8:
                monthString = "September";
                break;
            case 9:
                monthString = "Oktober";
                break;
            case 10:
                monthString = "November";
                break;
            case 11:
                monthString = "Desember";
                break;
            default:
                monthString = "Invalid month";
                break;
        }
        return monthString;
    }

    public Date getNextDate(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, inc);
        return c.getTime();
    }

    public String formatHtmlToString(String html) {
        String string = "";
        if (html != null) {
            string = html.replace("<br>", "\n");
        }
        return string;
    }

    public void timer(Runnable runnable, Integer time) {
        Handler handler = new Handler();
        handler.postDelayed(runnable, time);
    }

    public OnClickListener onClick(final Runnable runnable) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, 0);
            }
        };
    }

    protected OnClickListener onClick(final Runnable runnable,
                                      final Integer timer) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, timer);
            }
        };
    }


    protected void DirectoryCreate(String directory) {
        File dir = new File(directory);
        dir.mkdirs();
    }

    protected void DirectoryClear(String directory) {
        File dir = new File(directory);

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                DirectoryDelete(new File(dir, children[i]));
            }
        }
    }

    protected void chaceClear() {
        try {
            File dir = this.getContext().getCacheDir();
            if (dir != null && dir.isDirectory()) {
                DirectoryDelete(dir);
            }
        } catch (Exception e) {
        }
    }

    protected static boolean DirectoryDelete(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = DirectoryDelete(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    protected int getScreenWidth() {
        WindowManager wm = (WindowManager) AppController.getAppContext()
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        return metrics.widthPixels;
    }

    protected int getScreenHeight() {

        WindowManager wm = (WindowManager) AppController.getAppContext()
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        return metrics.heightPixels;
    }

    protected Double getNumberFormatExponential(Number number) {
        return Double.parseDouble(number + "");
    }

    protected String getNumberFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("##", symbols);
        String hasil = formatter.format(number);

        return hasil;
    }

    protected String getMoneyFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
        }
//        return "Rp. " + hasil;
        return hasil;
    }

    protected String getMoneyFormatDollar(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
        }
        return "$. " + hasil;
    }


    protected void keyboardShow(View view) {

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        view.requestFocus();
//        InputMethodManager imm = (InputMethodManager) getContext()
//                .getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(view, 0);
    }

    protected void keyboardHide(View view) {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void keyboardTogle() {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public String stringToUrl(String text) {
        String urlValidationRegex = "(https?|ftp)://(www\\d?|[a-zA-Z0-9]+)?.[a-zA-Z0-9-]+(\\:|.)([a-zA-Z0-9.]+|(\\d+)?)([/?:].*)?";
        Pattern p = Pattern.compile(urlValidationRegex);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String found = m.group(0);
            m.appendReplacement(sb, "<a href='" + found + "' target='_blank'>" + found + "</a>");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    protected String getThousandFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
        return hasil;
    }

    public Integer convertDipToPixel(int dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics));
    }
}
