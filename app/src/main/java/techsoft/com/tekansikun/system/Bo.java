package techsoft.com.tekansikun.system;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pangeran Visione on 8/18/2017.
 */

public class Bo {

    private JSONObject JObject;

    public void setBoObject(JSONObject JObject) {
        this.JObject = JObject;
    }

    public String getString(String field){
        String value = "";

        if(!JObject.isNull(field)){
            try {
                value = JObject.getString(field);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return value;
    }

    public Integer getInt(String field){
        Integer value = 0;

        if(!JObject.isNull(field)){
            try {
                value = JObject.getInt(field);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return value;
    }

    public Long getLong(String field){
        Long value = 0l;

        if(!JObject.isNull(field)){
            try {
                value = JObject.getLong(field);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return value;
    }

    public Double getDouble(String field){
        Double value = 0d;

        if(!JObject.isNull(field)){
            try {
                value = JObject.getDouble(field);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return value;
    }
}
