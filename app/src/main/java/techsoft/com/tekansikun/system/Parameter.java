package techsoft.com.tekansikun.system;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Parameter implements Serializable {

	private String name = "";
	private String value = "";
	private Object object;

	public Parameter set(String name, String value){
		Parameter parameter = new Parameter();
		parameter.setName(name);
		parameter.setValue(value);
		return parameter;
	}

	public Parameter set(String name, Object Object){
		Parameter parameter = new Parameter();
		parameter.setName(name);
		parameter.setObject(object);
		return parameter;
	}

	public Parameter set(String name, String value, Object Object){
		Parameter parameter = new Parameter();
		parameter.setName(name);
		parameter.setValue(value);
		parameter.setObject(object);
		return parameter;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}


}
