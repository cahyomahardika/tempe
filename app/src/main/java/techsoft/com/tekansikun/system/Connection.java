package techsoft.com.tekansikun.system;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import techsoft.com.tekansikun.system.util.Utils;


// http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng=-7.5512562,110.7879732&language=en

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("SimpleDateFormat")
public class Connection {

    private Boolean writeLog = false;
    private Bitmap image;
    private String url = "";
    private String data = "";

    private Response response;
    private MapAddress mapAddress;
    private Runnable onPreConnect, onResponse, onCancelConnect;
    private List<Parameter> parameters = new ArrayList<Parameter>();
    private Runnable taskCanceler;
    private Connect connect;

    public static final int MY_SOCKET_TIMEOUT_MS = 5000;
    public static final int DEFAULT_MAX_RETRIES = 3;

    public class Connect extends AsyncTask<String, Void, Response> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (onPreConnect != null) {
                onPreConnect.run();
            }
        }

        @Override
        protected Response doInBackground(String... urls) {
            try {
                return doResponse(Http.post(url, parameters).trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Response respon) {
            if (respon != null) {
                response = respon;
                if (onResponse != null) {
                    onResponse.run();
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            if (onCancelConnect != null) {
                onCancelConnect.run();
            }
        }

    }

    private class doConnectBitmap extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL urlImage = new URL(url);

                HttpURLConnection connection = (HttpURLConnection) urlImage
                        .openConnection();
                connection.setRequestProperty("connection", "close");
                connection.setDoInput(true);
                connection.connect();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream stream = connection.getInputStream();
                    BufferedInputStream streamBuffered = new BufferedInputStream(
                            stream);
                    System.gc();
                    return BitmapFactory.decodeStream(streamBuffered, null,
                            getBitmapFactoryOptions());
                } else {
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap gambar) {
            if (gambar != null) {
                image = gambar;

                if (onResponse != null) {
                    onResponse.run();
                }
            }
        }
    }

    private class doConnectMaps extends AsyncTask<String, Void, MapAddress> {
        @Override
        protected MapAddress doInBackground(String... urls) {
            try {
                return doMapAddress(Http.post(url, parameters).trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(MapAddress mapAddres) {
            if (mapAddres != null) {
                mapAddress = mapAddres;
                if (onResponse != null) {
                    onResponse.run();
                }
            }
        }
    }

    public static Bitmap getImage(byte[] image) {
        BitmapFactory.Options config = new BitmapFactory.Options();
        config.inPreferredConfig = Bitmap.Config.RGB_565;
        config.inSampleSize = 4;
        return BitmapFactory.decodeByteArray(image, 0, image.length, config);
    }

    private String decoder(String string) {
        try {
            byte[] valueDecoded = Base64.decode(new String(string).getBytes(),
                    Base64.DEFAULT);
            return new String(valueDecoded);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void longInfo(String data) {
        if (data.length() > 4000) {
            Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                    + " Connection", data.substring(0, 4000));
            longInfo(data.substring(4000));
        } else
            Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                    + " Connection", data);
    }

    private Response doResponse(String data) {
        this.data = data;

        if (writeLog) {
            longInfo(data);
        }

        try {
            if (writeLog) {
                Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                        + " Connection", "Response Reading ..");
            }

            JSONObject obj = new JSONObject(data);
            if (obj != null) {
                Boolean connection = obj.getInt("result") == 1 ? true : false;

                if (connection) {
                    String message = obj.isNull("message") ? "" : obj
                            .getString("message");
                    String messageCode = obj.isNull("messageCode") ? "" : obj
                            .getString("messageCode");

                    Response response = new Response();
                    response.isValid(connection);
                    response.setMessage(message);
                    response.setCode(messageCode);
                    response.setData(data);
                    response.setObject(obj);


                    if (writeLog) {
                        Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                                + " Connection", "Response is valid");
                    }
                    System.gc();
                    return response;
                } else {
                    String message = obj.isNull("message") ? "" : obj
                            .getString("message");
                    String messageCode = obj.isNull("messageCode") ? "" : obj
                            .getString("messageCode");

                    Response response = new Response();
                    response.isValid(connection);
                    response.setMessage(message);
                    response.setCode(messageCode);
                    response.setData(data);
                    response.setObject(obj);

                    if (writeLog) {
                        Utils.logw(new SimpleDateFormat("hh:mm:sss")
                                        .format(new Date()) + " Connection",
                                "Response is't valid");
                    }
                    System.gc();
                    return response;
                }
            } else {
                Response response = new Response();
                response.isValid(false);
                response.setMessage("Response failed");
                response.setCode("405");
                response.setData(data);

                if (writeLog) {
                    Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                            + " Connection", "Response 404 not found");
                }
                System.gc();
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            longInfo(data);
            Response response = new Response();
            response.isValid(false);
            response.setMessage("Erorr parsing : " + e);
            response.setCode("405");
            response.setData(data);

            if (writeLog) {
                Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                        + " Connection", "Response error");
            }
            System.gc();
            return response;
        }
    }

    public static Response getResponseFromJSON(String data) {
        try {

             JSONObject obj = new JSONObject(data);
            if (obj != null) {
                Boolean connection = obj.getInt("result") == 1 ? true : false;

                if (connection) {
                    String message = obj.isNull("message") ? "" : obj
                            .getString("message");

                    Response response = new Response();
                    response.isValid(connection);
                    response.setMessage(message);
                    response.setData(data);
                    response.setObject(obj);
                    System.gc();
                    return response;
                } else {
                    String message = obj.isNull("message") ? "" : obj
                            .getString("message");

                    Response response = new Response();
                    response.isValid(connection);
                    response.setMessage(message);
                    response.setData(data);
                    response.setObject(obj);
                    System.gc();
                    return response;
                }
            } else {
                Response response = new Response();
                response.isValid(false);
                response.setMessage("Response failed");
                response.setCode("405");
                response.setData(data);
                System.gc();
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            longInfo(data);
            Response response = new Response();
            response.isValid(false);
            response.setMessage("Erorr parsing : " + e);
            response.setCode("405");
            response.setData(data);
            System.gc();
            return response;
        }
    }

    private MapAddress doMapAddress(String data) {
        try {
            MapAddress address = new MapAddress();

            JSONObject object = new JSONObject(data);
            JSONArray resultArray = object.getJSONArray("results");
            for (int i = 0; i < resultArray.length(); i++) {
                JSONObject addressComponentObject = resultArray
                        .getJSONObject(i);

                JSONArray componentArray = addressComponentObject
                        .getJSONArray("address_components");
                for (int j = 0; j < componentArray.length(); j++) {
                    JSONObject componentObject = componentArray
                            .getJSONObject(j);

                    JSONArray typeArray = componentObject.getJSONArray("types");
                    for (int k = 0; k < typeArray.length(); k++) {

                        if (typeArray.get(0).equals("route")) {
                            if (address.getAddress() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setAddress(componentObject
                                            .getString("long_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals("postal_code")) {
                            if (address.getPostal() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setPostal(componentObject
                                            .getString("long_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals("locality")) {
                            if (address.getCity() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setCity(componentObject
                                            .getString("long_name"));
                                }
                            }

                            if (address.getCityAlias() == "") {
                                if (componentObject.getString("short_name") != "") {
                                    address.setCityAlias(componentObject
                                            .getString("short_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals(
                                "administrative_area_level_3")) {
                            if (address.getDistric() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setDistric(componentObject
                                            .getString("long_name"));
                                }
                            }

                            if (address.getDistricAlias() == "") {
                                if (componentObject.getString("short_name") != "") {
                                    address.setDistricAlias(componentObject
                                            .getString("short_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals(
                                "administrative_area_level_2")) {
                            if (address.getCity() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setCity(componentObject
                                            .getString("long_name"));
                                }
                            }

                            if (address.getCityAlias() == "") {
                                if (componentObject.getString("short_name") != "") {
                                    address.setCityAlias(componentObject
                                            .getString("short_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals(
                                "administrative_area_level_1")) {
                            if (address.getProvince() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setProvince(componentObject
                                            .getString("long_name"));
                                }
                            }

                            if (address.getProvince() == "") {
                                if (componentObject.getString("short_name") != "") {
                                    address.setProvince(componentObject
                                            .getString("short_name"));
                                }
                            }
                        }

                        if (typeArray.get(0).equals("country")) {
                            if (address.getCountry() == "") {
                                if (componentObject.getString("long_name") != "") {
                                    address.setCountry(componentObject
                                            .getString("long_name"));
                                }
                            }

                            if (address.getCountryAlias() == "") {
                                if (componentObject.getString("short_name") != "") {
                                    address.setCountryAlias(componentObject
                                            .getString("short_name"));
                                }
                            }
                        }
                    }
                }
            }

            return address;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public Connect post(String controller, String action,
                        List<Parameter> parameters) {
        try {
            this.url = Lang.API_URL + action;
            this.parameters = parameters;

            if (writeLog) {
                Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                                + " Connection",
                        "-------------------------------------------------------");
                Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                        + " Connection", "Connect to " + url);
                Utils.logw(new SimpleDateFormat("hh:mm:sss").format(new Date())
                                + " Connection",
                        "-------------------------------------------------------");
            }

            connect = new Connect();
            taskCanceler = new TaskCanceler(connect);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                connect.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                connect.execute();
            }

            return connect;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Connect getConnect() {
        return connect;
    }

    class TaskCanceler implements Runnable {

        private Connect task;

        public TaskCanceler(Connect task) {
            this.task = task;
        }

        @Override
        public void run() {
            if (task.getStatus() == AsyncTask.Status.RUNNING) {

                Response respon = new Response();
                respon.setCode("8989");
                respon.setMessage("Koneksi Time out");
                respon.setData("0");
                respon.setObject(new JSONObject());
                response = respon;

                task.cancel(true);
            }
        }

    }

    public String get(String controller, String action,
                      List<Parameter> parameters) throws Exception {
        String var = "connect=true";

        for (Parameter parameter : parameters) {
            var = var + "&" + parameter.getName() + "=" + parameter.getValue();
        }

        String url = Lang.BASE_URL + "/" + controller + "/" + action + "?"
                + var;
        return Http.get(url).trim();
    }

    public void address(Double latitude, Double longitude) {
        try {
            this.url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng="
                    + String.valueOf(latitude)
                    + ","
                    + String.valueOf(longitude) + "&language=en";
            new doConnectMaps().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setImage(String url) {
        try {
            this.url = url;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new doConnectBitmap()
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new doConnectBitmap().execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private BitmapFactory.Options getBitmapFactoryOptions() {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        // bitmapOptions.inJustDecodeBounds = true;
        bitmapOptions.inDither = false;
        bitmapOptions.inPurgeable = true;
        bitmapOptions.inInputShareable = true;
        // bitmapOptions.inTempStorage=new byte[32 * 1024];

        return bitmapOptions;
    }

    public void setOnResponse(Runnable onResponse) {
        this.onResponse = onResponse;
    }

    public Response getResponse() {
        return this.response;
    }

    public Runnable getOnPreConnect() {
        return onPreConnect;
    }

    public void setOnPreConnect(Runnable onPreConnect) {
        this.onPreConnect = onPreConnect;
    }

    public Runnable getOnCancelConnect() {
        return onCancelConnect;
    }

    public void setOnCancelConnect(Runnable onCancelConnect) {
        this.onCancelConnect = onCancelConnect;
    }

    public Bitmap getImage() {
        return image;
    }

    public MapAddress getMapAddress() {
        return this.mapAddress;
    }

    public String getData() {
        return data;
    }
}