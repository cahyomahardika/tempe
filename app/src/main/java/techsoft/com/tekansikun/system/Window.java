package techsoft.com.tekansikun.system;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reactiveandroid.Model;
import com.reactiveandroid.query.Delete;
import com.reactiveandroid.query.Select;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.relex.circleindicator.CircleIndicator;
import techsoft.com.tekansikun.database.model.Device;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressWarnings("rawtypes")
public class Window extends AppCompatActivity implements Lang {
    protected Visione visi = new Visione();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstanceState = null;
        super.onCreate(savedInstanceState);

        visi.setContext(getContext());
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateComponent();
    }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        this.requestWindowFeature(1);

        initPrivateObject();
        initComponent();
        initEventListener();
    }

    protected void initPrivateObject() {
    }

    protected void initComponent() {
    }

    protected void initEventListener() {
    }

    protected void updateComponent() {
    }


    /*
     * ------------------------------- CODE -----------------------------------------
     */
    public void view(int id) {
        setContentView(id);
    }

//    protected Device getDevice() {
//        return new Select().from(Device.class).executeSingle();
//    }
//
//    protected User getUser() {
//        return new Select().from(User.class).executeSingle();
//    }

    protected int sizeTableContent(Class<? extends Model> table) {
        return Select.from(table).fetch().size();
    }

    protected void deleteAll(Class<? extends Model> table) {
        Delete.from(table).execute();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void viewWebsite(WebView browser, String url) {
        browser.setWebViewClient(new WebViewClient());
        browser.setWebChromeClient(new WebChromeClient());
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setBuiltInZoomControls(true);
        browser.loadUrl(url);
    }

    public void controller(Class clas) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void controller(Class clas, List<Parameter> parameters) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter.getValue());
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        startActivity(intent);
    }

    public void controller(Class clas, Parameter... parameters) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter);
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        startActivity(intent);
    }

    public void controller(Fragment fragment, Integer containerID) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .replace(containerID, fragment, "previous");
        transaction.commit();
    }

    public void controllerNoAction(Class clas) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void controllerNoAction(Class clas, List<Parameter> parameters) {
        Intent intent = new Intent(this.getBaseContext(), clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter.getValue());
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        startActivity(intent);
    }


    protected boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    protected boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    protected boolean isValidEmailAddress(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    protected void showTooltip(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void showInformation(String message) {
        showConfirm(this.getContext(), "Informasi", message, "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
    }

    public static void showInformationSelectable(Activity activity, String message, String yes, DialogInterface.OnClickListener onClickListener, String no,
                                                 DialogInterface.OnClickListener onClickListener2) {
        TextView showText = new TextView(activity);
        showText.setText(message);
        showText.setGravity(Gravity.CENTER);
        showText.setTextAppearance(activity, android.R.style.TextAppearance_Medium);
        showText.setTextIsSelectable(true);
        showText.setPadding(50, 20, 50, 20);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setPositiveButton(yes, onClickListener);
        if (onClickListener2 != null) {
            builder.setNegativeButton(no, onClickListener2);
        } else {
            builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
        }
        builder.setView(showText)
                .setTitle("Informasi")
                .setCancelable(true)
                .show();
    }

    protected void showInformation(Context context, String judul, String pesan,
                                   String buttonLabel) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setNeutralButton(buttonLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alert.show();
    }


    protected void showConfirm(Context context, String judul, String pesan,
                               String labelYes, DialogInterface.OnClickListener clickListenerYes) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setPositiveButton(labelYes, clickListenerYes);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    protected void showConfirm(Context context, String judul, String pesan,
                               String labelYes, String labelNo,
                               DialogInterface.OnClickListener clickListenerYes,
                               DialogInterface.OnClickListener clickListenerNo) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setPositiveButton(labelYes, clickListenerYes);
        alert.setNegativeButton(labelNo, clickListenerNo);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    public Object getParameter(String string) {
        return getIntent().getExtras().get(string);
    }

    public Button getButton(int id) {
        return (Button) findViewById(id);
    }

    public CheckBox getCheckBox(int id) {
        return (CheckBox) findViewById(id);
    }

    public TextView getTextView(int id) {
        return (TextView) findViewById(id);
    }

    public EditText getEditText(int id) {
        return (EditText) findViewById(id);
    }

    public ImageView getImageView(int id) {
        return (ImageView) findViewById(id);
    }

    public ProgressBar getProgressBar(int id) {
        return (ProgressBar) findViewById(id);
    }

    public LinearLayout getLinearLayout(int id) {
        return (LinearLayout) findViewById(id);
    }

    public RelativeLayout getRelativeLayout(int id) {
        return (RelativeLayout) findViewById(id);
    }

    public ScrollView getScrollView(int id) {
        return (ScrollView) findViewById(id);
    }

    public Spinner getSpinner(int id) {
        return (Spinner) findViewById(id);
    }

    public WebView getWebView(int id) {
        return (WebView) findViewById(id);
    }

    public RecyclerView getRecyclerView(int id){
        return (RecyclerView) findViewById(id);
    }
    public ViewPager getPagerView(int id) {
        return (ViewPager) findViewById(id);
    }

    public CircleIndicator getCircleIndicator(int id) {
        return (CircleIndicator) findViewById(id);
    }
    public FrameLayout getFrameLayout(int id) {
        return (FrameLayout) findViewById(id);
    }

    @SuppressWarnings("static-access")
    public int color(String color) {
        return new Color().parseColor(color);
    }

    public String encode(String string) {
        byte[] bytesEncoded = Base64
                .encode(new String(string).getBytes(), 1000);
        return new String(bytesEncoded).trim();
    }

    public String decoder(String string) {
        byte[] valueDecoded = Base64
                .decode(new String(string).getBytes(), 1000);
        return new String(valueDecoded).trim();
    }

    @SuppressLint("SimpleDateFormat")
    public void log(String string) {
        Log.w("Bonoboapp.com "
                + new SimpleDateFormat("hh:mm:sss").format(new Date()), string);
    }

    public ArrayAdapter<Object> addSpinner(List<Object> datas) {
        ArrayAdapter<Object> dataAdapter = new ArrayAdapter<Object>(this,
                android.R.layout.simple_spinner_dropdown_item, datas);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return dataAdapter;
    }

    private int getDate4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tanggal = cal.get(Calendar.DATE);
        return tanggal;
    }

    private int getMonth4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int bulan = cal.get(Calendar.MONTH);
        return bulan;
    }

    private int getYear4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tahun = cal.get(Calendar.YEAR);
        return tahun;
    }

    @SuppressLint("SimpleDateFormat")
    public String getDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(date);
    }

    public String getDateString(Date date) {
        return getDay4String(date) + ", " + getDate4Integer(date) + " "
                + getMonth4String(date) + " " + getYear4Integer(date);
    }

    public String getDateStringMessage(Date date) {
        if (date == null) {
            date = new Date();
        }

        if (!getDate(date).equals(getDate(new Date()))) {
            return getDate4Integer(date) + " " + getMonth4String(date) + " "
                    + getYear4Integer(date);
        } else {
            return getTime(date);
        }
    }

    @SuppressLint("SimpleDateFormat")
    public String getTime(Date date) {
        DateFormat df = new SimpleDateFormat("hh:mm aa");
        return df.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public String getDateDatabase(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public String getDay4String(Date date) {
        String day = "Senin";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayInt = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayInt) {
            case 1:
                day = "Minggu";
                break;
            case 2:
                day = "Senin";
                break;
            case 3:
                day = "Selasa";
                break;
            case 4:
                day = "Rabu";
                break;
            case 5:
                day = "Kamis";
                break;
            case 6:
                day = "Jumat";
                break;
            case 7:
                day = "Sabtu";
                break;
        }

        return day;
    }

    public String getMonth4String(Date date) {
        String monthString = "";
        switch (getMonth4Integer(date)) {
            case 0:
                monthString = "Januari";
                break;
            case 1:
                monthString = "Februari";
                break;
            case 2:
                monthString = "Maret";
                break;
            case 3:
                monthString = "April";
                break;
            case 4:
                monthString = "Mei";
                break;
            case 5:
                monthString = "Juni";
                break;
            case 6:
                monthString = "Juli";
                break;
            case 7:
                monthString = "Augustus";
                break;
            case 8:
                monthString = "September";
                break;
            case 9:
                monthString = "Oktober";
                break;
            case 10:
                monthString = "November";
                break;
            case 11:
                monthString = "Desember";
                break;
            default:
                monthString = "Invalid month";
                break;
        }
        return monthString;
    }

    public Date getNextDate(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, inc);
        return c.getTime();
    }

    protected Handler timer(Runnable runnable, Integer time) {
        Handler handler = new Handler();
        handler.postDelayed(runnable, time);
        return handler;
    }

    protected OnClickListener onClick(final Runnable runnable) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, 10);
            }
        };
    }

    protected OnClickListener onClick(final Runnable runnable,
                                      final Integer timer) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, timer);
            }
        };
    }

    @SuppressWarnings("unused")
    public Context getContext() {
        if (this != null) {
            return this;
        } else {
            return AppController.getAppContext();
        }
    }

    public String formatHtmlToString(String html) {
        String string = "";
        if (html != null) {
            string = html.replace(" (br) ", "\r\n");
        }
        return string;
    }

    protected void chaceClear() {
        try {
            File dir = this.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                DirectoryDelete(dir);
            }
        } catch (Exception e) {
        }
    }

    protected void DirectoryCreate(String directory) {
        File dir = new File(directory);
        dir.mkdirs();
    }

    protected void DirectoryClear(String directory) {
        File dir = new File(directory);

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                DirectoryDelete(new File(dir, children[i]));
            }
        }
    }

    protected static boolean DirectoryDelete(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = DirectoryDelete(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }


    protected int getScreenWidth() {
        WindowManager wm = (WindowManager) getContext().getSystemService(
                Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        return metrics.widthPixels;
    }

    protected int getScreenHeight() {

        WindowManager wm = (WindowManager) getContext().getSystemService(
                Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        return metrics.heightPixels;
    }

    protected String getNumberFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("######.##", symbols);
        String hasil = formatter.format(number);

        return hasil;
    }

    protected String getMoneyFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);

        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
        }
//        return "Rp. " + hasil;
        return hasil;
    }

    protected String getMoneyFormatDollar(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
        if (hasil.indexOf(",") == -1) {
//            hasil = hasil + ",-";
        }
        return "$. " + hasil;
    }

    protected void keyboardShow(View view) {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    protected void keyboardHide(View view) {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void keyboardTogle() {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /*
     * This function to converting a numeric float in to numeric pixel Ex :
     * convert DIP to pixel for Component
     */
    protected Integer convertFloatToPixel(Float f) {
        DisplayMetrics metrics = getContext().getResources()
                .getDisplayMetrics();

        float fpixels = metrics.density * f;
        int pixels = (int) (fpixels + 0.5f);

        return pixels;
    }

    /*
     * This function to converting a numeric float in to numeric pixel Ex :
     * convert DIP to pixel for Component TextView
     */
    protected Integer convertTextSizeToPixel(Float f) {
        DisplayMetrics metrics = getContext().getResources()
                .getDisplayMetrics();

        float fpixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                f, metrics);
        int pixels = (int) (fpixels + 0.5f);

        return pixels;
    }

    public String stringToUrl(String text) {
        String urlValidationRegex = "(https?|ftp)://(www\\d?|[a-zA-Z0-9]+)?.[a-zA-Z0-9-]+(\\:|.)([a-zA-Z0-9.]+|(\\d+)?)([/?:].*)?";
        Pattern p = Pattern.compile(urlValidationRegex);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String found = m.group(0);
            m.appendReplacement(sb, "<a href='" + found + "' target='_blank'>"
                    + found + "</a>");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public Integer convertDipToPixel(int dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics));
    }

    public RadioButton getRadioButton(int id) {
        return (RadioButton) this.findViewById(id);
    }

    protected String getThousandFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);
        return hasil;
    }

    public AutoCompleteTextView getaAutoCompleteTextView(int id){
        return (AutoCompleteTextView) this.findViewById(id);
    }

    public String getDateNoDayString(Date date) {
        return getDate4Integer(date) + " "
                + getMonth4String(date) + " " + getYear4Integer(date);
    }
    protected Device getDevice() {
        return Select.from(Device.class).fetchSingle();
    }
}
