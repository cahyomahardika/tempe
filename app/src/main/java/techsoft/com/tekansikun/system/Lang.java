package techsoft.com.tekansikun.system;

import android.os.Environment;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public interface Lang {

//    PRODUCTION 1.5
//  public static final String base_url = "http://www.bonoboapp.com/";
//    public static final String api_url = "http://www.bonoboapp.com/api_1_5/";

//  PRODUCTION 1.5
//    public static final String base_url = "http://192.168.0.253:81/bonobo/";
//    public static final String api_url = "http://192.168.0.253:81/bonoboapi_1_6/";

//    public static final String base_url = "http://192.168.0.131/bonobo-2.1-web/";
//    public static final String api_url = "http://192.168.0.131/bonobo-2.1-web/api_1_6/";

//    public static final String base_url = "http://192.168.0.252/visione/bonobo-2.1/";
//    public static final String api_url = "http://192.168.0.252/visione/bonobo-2.1/api_1_6/";

//    public static final String base_url = "http://192.168.0.16/bonobo_api/";
//    public static final String api_url = "http://192.168.0.16/bonobo_api/api_1_6/";

////  TESTING XACLOUD
//    public static final String base_url = "http://bonobo.xacloud.com/";
//    public static final String api_url = "http://bonobo.xacloud.com/api_1_6/";

    //TESTING XACLOUD
//    public static final String base_url = "http://13.250.116.172/";
//    public static final String api_url = "http://13.250.116.172/api_1_6/";

    // PRODUCTION
//    public static final String base_url = "http://api.bonoboapp.com/";
//    public static final String api_url = "http://api.bonoboapp.com/api_1_6/";

 //   public static final String gcm_url = "http://www.bonoboapp.com/";
  //  public static final String BASE_URL = "http://13.250.116.172/api_1_6/";

    public static final String BASE_URL = "https://tempeikansalatiga.gkjbaki.org/";
    public static final String API_URL = BASE_URL+"api/";
    public static final String IMG_PROD_URL = BASE_URL + "foto_produk/";

    public static final String BASE_URL_RAJAONGKIR = "https://api.rajaongkir.com/starter/";
    public static final String API_KEY_RAJAONGKIR = "bf7d1bb246de310dd2bddb1600215b81";

    public static final String API_GCM_SENDER = "1036991435997"; //edited 5 Oktober 2018

    public static final String API_KEY = "api_key";
    public static final String API_VERSION = "api_version";

    public static final String APP_ID = "NOTA-API-1.1";
    public static final String APP_VERSION = "2";
    public static final String APP_ID_FACEBOOK = "1058125220883314";

    public static final String DB_NAME = "tempeikan_1";
    public static final int DB_VERSION = 3;

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String PATH_USER = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/user/";
    public static final String PATH_SHOP = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/shop/";
    public static final String PATH_SHOP_HIGH = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/shop/high/";
    public static final String PATH_PRODUCT = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/product/";

    public static final String PATH_LOG = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/";
    public static final String PATH_PRODUCT_HIGH = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/product/high/";
    public static final String PATH_PRODUCT_SAVED = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/nota/saved/";
    public static final String PATH_MESSAGE = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/message/";
    public static final String PATH_INVOICE = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/pic/invoice/";
    public static final String PATH_BACKUP_DATABASE = Environment
            .getExternalStorageDirectory() == null ? Environment
            .getDataDirectory().toString() : Environment
            .getExternalStorageDirectory().toString()
            + "/Android/data/visione.com.nota/assets/backup/database/";

    public static final int VISIBLE_TRUE = 0;
    public static final int VISIBLE_FALSE = View.INVISIBLE;
    public static final int VISIBLE_GONE = View.GONE;
    public static final int FILL_PARENT = LayoutParams.MATCH_PARENT;
    public static final int WRAP_CONTENT = LayoutParams.WRAP_CONTENT;
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    public static final int FONT_TINY = 12;
    public static final int FONT_SMALL = 14;
    public static final int FONT_MEDIUM = 18;
    public static final int FONT_BIG = 21;

    public static final String COLOR_WHITE = "#ffffff";
    public static final String COLOR_WHITE_HOVER = "#fafafa";
    public static final String COLOR_BLACK = "#111111";
    // public static final String COLOR_BLACK_HOVER = "#313131";
    public static final String COLOR_BLUE = "#4096ee";
    public static final String COLOR_BLUE_HOVER = "#9AC3ED";
    public static final String COLOR_CYAM = "#efefef";
    public static final String COLOR_CYAM_HOVER = "#e5e5e5";
    public static final String COLOR_ORANGE = "#ff5722";
    public static final String COLOR_ORANGE_HOVER = "#ff6922";
    public static final String DARK_GREY = "#aaaaaa";

   /* PRODUCT LANG*/
   public static final Integer PRICE_FLAG_PRODUCT = 0;
   public static final Integer PRICE_FLAG_VARIAN = 1;

    /* flag user login */
    public static final int LOGIN = 0;
    public static final int LOGOUT = 1;

    /* flag user product */
    public static final int PRODUCT_STOCK_PREORDER = 0;
    public static final int PRODUCT_STOCK_READY = 1;

    public static final String BROADCAST_NOTA_SELLER = "broadcast_nota_seller";
    public static final String BROADCAST_NOTIFIKASI_NOTA_SELLER = "broadcast_notifikasi_nota_seller";

    public static final String BROADCAST_CART = "broadcast_cart";
    public static final String BROADCAST_PRODUCT = "broadcast_product";
}
