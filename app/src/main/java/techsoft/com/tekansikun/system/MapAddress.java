package techsoft.com.tekansikun.system;

public class MapAddress {
	private Double latitude = 0d;
	private Double longitude = 0d;
	private String postal = "";
	private String address = "";
	private String distric = "";
	private String districAlias = "";
	private String city = "";
	private String cityAlias = "";
	private String province = "";
	private String provinceAlias = "";
	private String country = "";
	private String countryAlias = "";
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDistric() {
		return distric;
	}
	public void setDistric(String distric) {
		this.distric = distric;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCityAlias() {
		return cityAlias;
	}
	public void setCityAlias(String cityAlias) {
		this.cityAlias = cityAlias;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getProvinceAlias() {
		return provinceAlias;
	}
	public void setProvinceAlias(String provinceAlias) {
		this.provinceAlias = provinceAlias;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountryAlias() {
		return countryAlias;
	}
	public void setCountryAlias(String countryAlias) {
		this.countryAlias = countryAlias;
	}
	public String getDistricAlias() {
		return districAlias;
	}
	public void setDistricAlias(String districAlias) {
		this.districAlias = districAlias;
	}
	
	
}
