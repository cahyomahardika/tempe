package techsoft.com.tekansikun.system;

import com.reactiveandroid.annotation.Database;

import static techsoft.com.tekansikun.system.Lang.DB_NAME;
import static techsoft.com.tekansikun.system.Lang.DB_VERSION;


/**
 * Created by USER on 29/01/2019.
 */

@Database(name = DB_NAME, version = DB_VERSION)
public class AppDatabase {

}