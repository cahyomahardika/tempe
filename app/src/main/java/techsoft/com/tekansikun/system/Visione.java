package techsoft.com.tekansikun.system;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.reactiveandroid.Model;
import com.reactiveandroid.query.Delete;
import com.reactiveandroid.query.Select;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Visione {
    public static Context context;
    private ProgressDialog loader;
    private static Boolean isLog = true;

    public void setContext(Context context) {
        this.context = context;

//        initDatabase();

//        new DatabaseContentProvider();
    }

    /*
    *   --------------------------------------------------------------------------
    *   DATABASE
    *   --------------------------------------------------------------------------
    */

//    public User getUser() {
//        return new Select().from(User.class).executeSingle();
//    }

    protected static void deleteAll(Class<? extends Model> table) {
        Delete.from(table).execute();
    }

//    public void initDatabase(){
//        Configuration dbConfiguration = new Configuration.Builder(context)
//                .setDatabaseName(Lang.DB_NAME)
//                .setDatabaseVersion(Lang.DB_VERSION)
//                .addModelClasses(
////                        Patient.class
//                        Produk.class
//                )
//                .create();
//        ActiveAndroid.initialize(dbConfiguration);
//    }

    /*
    *   --------------------------------------------------------------------------
    *   CONNECTION INTERNET
    *   --------------------------------------------------------------------------
    */

    public static boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    /*
    *   --------------------------------------------------------------------------
    *   DIRECTORY
    *   --------------------------------------------------------------------------
    */

    public static void directoryCreate(String directory) {
        File dir = new File(directory);
        dir.mkdirs();
    }

    public static void directoryChaceClear() {
        try {
            AsyncTask async = new AsyncTask() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                }

                @Override
                protected Object doInBackground(Object[] params) {
                    File dir = context.getCacheDir();
                    if (dir != null && dir.isDirectory()) {
                        directoryDelete(dir);
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);

                }
            };

            async.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void directoryClear(String directory) {
        File dir = new File(directory);

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                directoryDelete(new File(dir, children[i]));
            }
        }
    }

    public static boolean directoryDelete(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = directoryDelete(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    /*
    *   --------------------------------------------------------------------------
    *   DATE AND TIMER
    *   --------------------------------------------------------------------------
    */

    public static Handler timer(Runnable runnable, Integer time) {
        Handler handler = new Handler();
        handler.postDelayed(runnable, time);
        return handler;
    }

    private int getDate4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tanggal = cal.get(Calendar.DATE);
        return tanggal;
    }

    private int getMonth4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int bulan = cal.get(Calendar.MONTH);
        return bulan;
    }

    private int getYear4Integer(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int tahun = cal.get(Calendar.YEAR);
        return tahun;
    }

    public String getDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(date);
    }

    public String getDateString(Date date) {
        return getDay4String(date) + ", " + getDate4Integer(date) + " "
                + getMonth4String(date) + " " + getYear4Integer(date);
    }

    public String getDateStringMessage(Date date) {
        if (date == null) {
            date = new Date();
        }

        if (!getDate(date).equals(getDate(new Date()))) {
            return getDate4Integer(date) + " " + getMonth4String(date) + " "
                    + getYear4Integer(date);
        } else {
            return getTime(date);
        }
    }

    public String getTime(Date date) {
        DateFormat df = new SimpleDateFormat("hh:mm aa");
        return df.format(date);
    }

    public String getDateDatabase(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public String getDateTimeString(Date date) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        return df.format(date);
    }

    public String getDay4String(Date date) {
        String day = "Senin";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayInt = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayInt) {
            case 1:
                day = "Minggu";
                break;
            case 2:
                day = "Senin";
                break;
            case 3:
                day = "Selasa";
                break;
            case 4:
                day = "Rabu";
                break;
            case 5:
                day = "Kamis";
                break;
            case 6:
                day = "Jumat";
                break;
            case 7:
                day = "Sabtu";
                break;
        }

        return day;
    }

    public String getMonth4String(Date date) {
        String monthString = "";
        switch (getMonth4Integer(date)) {
            case 0:
                monthString = "Januari";
                break;
            case 1:
                monthString = "Februari";
                break;
            case 2:
                monthString = "Maret";
                break;
            case 3:
                monthString = "April";
                break;
            case 4:
                monthString = "Mei";
                break;
            case 5:
                monthString = "Juni";
                break;
            case 6:
                monthString = "Juli";
                break;
            case 7:
                monthString = "Augustus";
                break;
            case 8:
                monthString = "September";
                break;
            case 9:
                monthString = "Oktober";
                break;
            case 10:
                monthString = "November";
                break;
            case 11:
                monthString = "Desember";
                break;
            default:
                monthString = "Invalid month";
                break;
        }
        return monthString;
    }

    public Date getNextDate(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, inc);
        return c.getTime();
    }

    public Date getNextMonth(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, inc);
        return c.getTime();
    }

    public Date getNextYear(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, inc);
        return c.getTime();
    }

    public Date getNextHour(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR, inc);
        return c.getTime();
    }

    public Date getNextMinute(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, inc);
        return c.getTime();
    }

    public Date getNextSecond(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND, inc);
        return c.getTime();
    }

    public Date getNextMilisecond(Date date, Integer inc) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MILLISECOND, inc);
        return c.getTime();
    }



    /*
    *   --------------------------------------------------------------------------
    *   VIEW EVENTLISTENER
    *   --------------------------------------------------------------------------
    */

    public static View.OnClickListener onClick(final Runnable runnable) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, 10);
            }
        };
    }

    public static View.OnClickListener onClick(final Runnable runnable, final Integer timer) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer(runnable, timer);
            }
        };
    }

    public static void controller(Class clas) {
        Intent intent = new Intent(context, clas);
//        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void controller(Class clas, List<Parameter> parameters) {
        Intent intent = new Intent(context, clas);
//        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter.getValue());
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        context.startActivity(intent);
    }

    public static void controller(Class clas, Parameter... parameters) {
        Intent intent = new Intent(context, clas);
//        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter);
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        context.startActivity(intent);
    }

    public static void controllerNoAction(Class clas) {
        Intent intent = new Intent(context, clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }

    public static void controllerNoAction(Class clas, List<Parameter> parameters) {
        Intent intent = new Intent(context, clas);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        for (Parameter parameter : parameters) {
            if (parameter.getObject() != null) {
                intent.putExtra(parameter.getName(), parameter.getValue());
            } else {
                intent.putExtra(parameter.getName(), parameter.getValue());
            }
        }
        context.startActivity(intent);
    }

    public static void controller(Fragment fragment, Integer containerID) {
        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .replace(containerID, fragment, "previous");
        transaction.commit();
    }
    /*
    *   --------------------------------------------------------------------------
    *   UTILITY
    *   --------------------------------------------------------------------------
    */

    public void showTooltip(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public void showTooltip(String msg, Integer timeout) {
        Toast.makeText(context, msg, timeout).show();
    }

    public static void log(String string) {
        if(isLog) {
            Log.e("visione.com " + new SimpleDateFormat("hh:mm:sss").format(new Date()), string);
        }
    }

    public void showInformation(String message) {
        showConfirm(context, "Informasi", message, "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
    }

    public void showConfirm(Context context, String judul, String pesan,
                               String labelYes, DialogInterface.OnClickListener clickListenerYes) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);
        alert.setPositiveButton(labelYes, clickListenerYes);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    public void showConfirm(Context context, String judul, String pesan,
                               String labelYes, String labelNo,
                               DialogInterface.OnClickListener clickListenerYes,
                               DialogInterface.OnClickListener clickListenerNo) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(judul);
        alert.setMessage(pesan);
        alert.setCancelable(false);

        alert.setPositiveButton(labelYes, clickListenerYes);
        alert.setNegativeButton(labelNo, clickListenerNo);

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    public static int getScreenWidth() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static int getScreenHeight() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return metrics.heightPixels;
    }

    public String encode(String string) {
        byte[] bytesEncoded = Base64.encode(new String(string).getBytes(), 1000);
        return new String(bytesEncoded).trim();
    }

    public String getMoneyFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String hasil = formatter.format(number);

        if (hasil.indexOf(",") == -1) {
            hasil = hasil + ",-";
        }
        return "Rp. " + hasil;
    }

    public int sizeTableContent(Class<? extends Model> table) {
        return Select.from(table).fetch().size();
    }

    public Bitmap getVideoThumb(String filePath, int type){
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    private Bitmap getVideoThumbByTime(String filePath, int timeInSeconds){
        MediaMetadataRetriever mMMR = new MediaMetadataRetriever();
        mMMR.setDataSource(filePath);
        return mMMR.getFrameAtTime(timeInSeconds * 1000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
    }

    private Bitmap getImageThumb(String filePath, int width, int height){
        return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(filePath), width, height);
    }

    private Bitmap getImageThumb(Bitmap source, int width, int height){
        return ThumbnailUtils.extractThumbnail(source, width, height, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
    }

    public void loaderShow() {
        try {
            if (context != null) {
                loader = ProgressDialog.show(context, "", "Loading ..", false);
                loader.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loaderHide() {
        try {
            if (loader != null) {
                loader.hide();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sms(String to_phone, String message){
        int length = message.length();
        SmsManager smsManager = SmsManager.getDefault();
        if(length > 160) {
            ArrayList<String> messagelist = smsManager.divideMessage(message);
            smsManager.sendMultipartTextMessage(to_phone, null, messagelist, null, null);
        }else{
            smsManager.sendTextMessage(to_phone, null, message, null, null);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public void sms(String to_phone, String message, Integer subId){
        SmsManager smsManager = SmsManager.getSmsManagerForSubscriptionId(subId);
        smsManager.sendTextMessage(to_phone, null, message, null, null);
    }


//    public LatLng getLocation() {
//        try {
//            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//            Criteria criteria = new Criteria();
//            String bestProvider = locationManager.getBestProvider(criteria, false);
//            Location location = locationManager.getLastKnownLocation(bestProvider);
//            Double lat,lon;
//            lat = location.getLatitude ();
//            lon = location.getLongitude ();
//            return new LatLng(lat, lon);
//        }        catch (NullPointerException e){
//            e.printStackTrace();
//            return null;
//        }
//    }

    public String getNumberFormat(Number number) {
        if (number == null) {
            number = 0;
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("##,###.##", symbols);
        String hasil = formatter.format(number);

        return hasil;
    }

    @SuppressLint("SimpleDateFormat")
    public void saveImageToFile(String directory, String filename, final Bitmap bitmap) {
        try {
            File file = new File(directory, filename);
            FileOutputStream fOut = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);

            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public void saveImageToFile(final String directory, final String filename, final String url) {
        final Connection connection = new Connection();
        connection.setOnResponse(new Runnable() {
            @Override
            public void run() {
                try {
                    Bitmap bitmap = connection.getImage();
                    saveImageToFile(directory, filename, bitmap);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        connection.setImage(url);
    }

    public static void share(Context activity, String shareTitle, String shareBody, String imagePath) {
        File file = new File(imagePath);
        Uri uri = Uri.fromFile(file);

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
        emailIntent.putExtra(Intent.EXTRA_TITLE, shareTitle);
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.setType("message/rfc822");

        PackageManager pm = activity.getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/*");

        List<ResolveInfo> emailResInfo = pm.queryIntentActivities(emailIntent, 0);

        String packageNames = "";
        for (int i = 0; i < emailResInfo.size(); i++) {
            ResolveInfo ri = emailResInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            packageNames = packageNames + " " + packageName;
        }

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<>();
        String prevPackageName = "";
        for (int i = 0; i < resInfo.size(); i++) {
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (!prevPackageName.equals(packageName) && !packageNames.contains(packageName)) {


                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("image/*");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                intent.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
                intent.putExtra(Intent.EXTRA_TITLE, shareTitle);
                intent.putExtra(Intent.EXTRA_STREAM, uri);

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
            prevPackageName = packageName;
        }

        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        Intent openInChooserIntent = Intent.createChooser(emailIntent, "Bagikan via...");
        openInChooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        activity.startActivity(openInChooserIntent);
    }

//    public Double getProductPrice(ProductList product){
//        Double price = product.getPrice1();
//
//        if (product.getShop().isJoined() == 1) {
//            switch (product.getShop().getLevelPrice()) {
//                case 1:
//                    if (product.getPrice1() > 0 && product.getShop().isLevel1Active()) {
//                        price = product.getPrice1();
//                    }
//                    break;
//
//                case 2:
//                    if (product.getPrice2() > 0 && product.getShop().isLevel2Active()) {
//                        price = product.getPrice2();
//                    }
//                    break;
//
//                case 3:
//                    if (product.getPrice3() > 0 && product.getShop().isLevel3Active()) {
//                        price = product.getPrice3();
//                    }
//                    break;
//
//                case 4:
//                    if (product.getPrice4() > 0 && product.getShop().isLevel4Active()) {
//                        price = product.getPrice4();
//                    }
//                    break;
//
//                case 5:
//                    if (product.getPrice5() > 0 && product.getShop().isLevel5Active()) {
//                        price = product.getPrice5();
//                    }
//                    break;
//
//                default:
//                    price = product.getPrice1();
//                    break;
//            }
//        }
//
//        return price;
//    }

//    public Double getProductVarianPrice(ProductList product, ProductVarian varian){
//        Double price = varian.getPriceVarian1();
//
//        if(product.getShop().isJoined() == 1){
//            switch (product.getShop().getLevelPrice()){
//                case 1 :
//                    if (varian.getPriceVarian1() > 0 && product.getShop().isLevel1Active()) {
//                        price = varian.getPriceVarian1();
//                    }
//                    break;
//                case 2 :
//                    if (varian.getPriceVarian2() > 0 && product.getShop().isLevel2Active()) {
//                        price = varian.getPriceVarian2();
//                    }
//                    break;
//                case 3 :
//                    if (varian.getPriceVarian3() > 0 && product.getShop().isLevel3Active()) {
//                        price = varian.getPriceVarian3();
//                    }
//                    break;
//                case 4 :
//                    if (varian.getPriceVarian4() > 0 && product.getShop().isLevel4Active()) {
//                        price = varian.getPriceVarian4();
//                    }
//                    break;
//                case 5 :
//                    if (varian.getPriceVarian5() > 0 && product.getShop().isLevel5Active()) {
//                        price = varian.getPriceVarian5();
//                    }
//                    break;
//            }
//        }
//
//        return price;
//    }
}
