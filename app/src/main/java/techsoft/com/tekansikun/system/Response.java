package techsoft.com.tekansikun.system;

import org.json.JSONObject;

public class Response {
	private Boolean isValid  = false;
	private String code = "";
	private String message  = "";
	private String data  = "";
	private JSONObject object;
	
	
	public Boolean isValid() {
		return isValid;
	}
	
	public void isValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public JSONObject getObject() {
		return object;
	}
	
	public void setObject(JSONObject object) {
		this.object = object;
	}
	
	
}
