package techsoft.com.tekansikun.system;

import android.annotation.SuppressLint;
import android.os.StrictMode;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class Http {
    public static final int HTTP_TIMEOUT = 30 * 1000;
    private static HttpClient mHttpClient;

    public static HttpClient getHttpClient() {
        if (mHttpClient == null) {
            mHttpClient = new DefaultHttpClient();

            final HttpParams params = mHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
            ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);
            ClientConnectionManager mgr = mHttpClient.getConnectionManager();
            mHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
                    mgr.getSchemeRegistry()), params);
        }
        return mHttpClient;
    }

    private static List<NameValuePair> convertParameter(
            List<Parameter> parameters) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        for (final Parameter parameter : parameters) {
            NameValuePair nameValuePair = new NameValuePair() {
                public String getName() {
                    return parameter.getName();
                }

                public String getValue() {
                    return parameter.getValue();
                }
            };

            nameValuePairs.add(nameValuePair);
        }

        return nameValuePairs;
    }

    public static String post(String url, List<Parameter> postParameters)
            throws Exception {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        BufferedReader in = null;

        try {
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
                    convertParameter(postParameters));

            HttpClient client = getHttpClient();
            HttpPost request = new HttpPost(url);

            request.setEntity(formEntity);

            HttpResponse response = client.execute(request);
            InputStream is = response.getEntity().getContent();
            InputStreamReader isr = new InputStreamReader(is);
            in = new BufferedReader(isr);

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");

            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            String result = sb.toString();
            is.close();

            return result;
        } catch (ConnectTimeoutException cte) {
            return "{'result':0,'message':'Koneksi Timeout','messageCode':8989 }";
        } catch (Exception e) {
            e.printStackTrace();
            return "{'result':0,'message':'" + (e.getMessage() == null ? "Tidak dapat menjangkau server" : e.getMessage())
                    + "','messageCode':8989,'url':'" + url + "'}"; // Tadinya 111, tapi karena saya lihat belum ada handle apa2, saya set ke 8989
            // saja, supaya dianggap timout
        } catch (OutOfMemoryError e) {
            return "{'result':0,'message':'" + (e.getMessage() == null ? "Terjadi kesalahan penggunaan memory" : e.getMessage()) + "','messageCode':1111,'url':'" + url + "'}";
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String get(String url) throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(url));
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity()
                    .getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");

            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            String result = sb.toString();
            return result;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
