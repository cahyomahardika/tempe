package techsoft.com.tekansikun.database.bo;

import com.reactiveandroid.ReActiveAndroid;
import com.reactiveandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import techsoft.com.tekansikun.database.model.Produk;
import techsoft.com.tekansikun.system.AppDatabase;
import techsoft.com.tekansikun.system.Bo;

import static techsoft.com.tekansikun.system.Lang.IMG_PROD_URL;


public class BoProduk extends Bo {

    public Produk doSave(JSONObject JObject) {
        setBoObject(JObject);
        ReActiveAndroid.getDatabase(AppDatabase.class).beginTransaction();

        try {
            Produk object = new Produk();
            Produk QObject = Select.from(Produk.class).where("id_x = '" + JObject.getString("id_produk")+"'").fetchSingle();

            if (QObject != null) {
                object = QObject;
            }

            object.setIdX(getString("id_produk"));
            object.setNamaProduk(getString("nama_produk"));
            object.setHargaProduk(getString("harga_produk"));
            object.setBeratProduk(getString("berat_produk"));
            object.setFotoProduk(IMG_PROD_URL+getString("foto_produk"));
            object.setDeskripsiProduk(getString("deskripsi_produk"));
            object.setStokProduk(getString("stok_produk"));
            object.save();

            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            ReActiveAndroid.getDatabase(AppDatabase.class).getWritableDatabase().setTransactionSuccessful();
            ReActiveAndroid.getDatabase(AppDatabase.class).endTransaction();
        }
    }

    public List<Produk> doSave(JSONArray JObjects){
        List<Produk> objects = new ArrayList<Produk>();
        try {
            for (int i = 0; i < JObjects.length(); i++) {
                JSONObject JObject = JObjects.getJSONObject(i);
                Produk object = doSave(JObject);

                if(object != null){
                    objects.add(object);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return objects;
        }
    }
}
