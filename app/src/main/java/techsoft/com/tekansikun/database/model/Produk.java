package techsoft.com.tekansikun.database.model;



import com.reactiveandroid.Model;
import com.reactiveandroid.annotation.Column;
import com.reactiveandroid.annotation.PrimaryKey;
import com.reactiveandroid.annotation.Table;
import com.reactiveandroid.annotation.Unique;

import java.io.Serializable;

import techsoft.com.tekansikun.system.AppDatabase;

/**
 * Created by USER on 30/10/2018.
 */

@Table(name = "tb_produk", database = AppDatabase.class)
public class Produk extends Model implements Serializable {

    private static final long serialVersionUID = -4013360603769019258L;

    @PrimaryKey
    private Long id;

    @Unique
    @Column(name = "id_x")
    private String idX;

    @Column(name="nama_produk")
    private String namaProduk = "";

    @Column(name="harga_produk")
    private String hargaProduk = "";

    @Column(name="berat_produk")
    private String beratProduk ;

    @Column(name="deskripsi_produk")
    private String deskripsiProduk ;

    @Column(name="stok_produk")
    private String stokProduk ;

    @Column(name="foto_produk")
    private String fotoProduk ;

    public String getIdX() {
        return idX;
    }

    public void setIdX(String idX) {
        this.idX = idX;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getHargaProduk() {
        return hargaProduk;
    }

    public void setHargaProduk(String hargaProduk) {
        this.hargaProduk = hargaProduk;
    }

    public String getBeratProduk() {
        return beratProduk;
    }

    public void setBeratProduk(String beratProduk) {
        this.beratProduk = beratProduk;
    }

    public String getDeskripsiProduk() {
        return deskripsiProduk;
    }

    public void setDeskripsiProduk(String deskripsiProduk) {
        this.deskripsiProduk = deskripsiProduk;
    }

    public String getStokProduk() {
        return stokProduk;
    }

    public void setStokProduk(String stokProduk) {
        this.stokProduk = stokProduk;
    }

    public String getFotoProduk() {
        return fotoProduk;
    }

    public void setFotoProduk(String fotoProduk) {
        this.fotoProduk = fotoProduk;
    }
}
