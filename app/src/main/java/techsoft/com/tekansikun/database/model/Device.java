package techsoft.com.tekansikun.database.model;



import com.reactiveandroid.Model;
import com.reactiveandroid.annotation.Column;
import com.reactiveandroid.annotation.PrimaryKey;
import com.reactiveandroid.annotation.Table;

import java.io.Serializable;

import techsoft.com.tekansikun.system.AppDatabase;


@Table(name = "tb_device",database = AppDatabase.class)
public class Device extends Model implements Serializable {

    private static final long serialVersionUID = -2252776054991382399L;

    @PrimaryKey
    private Long id;

    @Column(name = "id_x")
    private Long idX;

    @Column(name="imei")
    private String imei = "";

    @Column(name="fcm")
    private String fcm = "";

    public Long getIdX() {
        return idX;
    }

    public void setIdX(Long idX) {
        this.idX = idX;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getFcm() {
        return fcm;
    }

    public void setFcm(String fcm) {
        this.fcm = fcm;
    }
}
