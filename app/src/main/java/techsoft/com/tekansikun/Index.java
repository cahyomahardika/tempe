package techsoft.com.tekansikun;

import android.content.Intent;
import android.os.Handler;

import techsoft.com.tekansikun.controllers.sign.SignIn;
import techsoft.com.tekansikun.system.Visione;
import techsoft.com.tekansikun.system.Window;

public class Index extends Window {
    private Boolean isDeviceFcmNew = false;
    Visione visione = new Visione();

    @Override
    protected void initPrivateObject() {
        super.initPrivateObject();
        chaceClear();
//        DirectoryCreate(PATH_LOG);
    }

    @Override
    protected void initComponent() {
        super.initComponent();
        view(R.layout.index);
        //prBar = getProgressBar(R.id.progress);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                controller(SignIn.class);
                finish();
            }
        }, 4000);
    }

    @Override
    protected void updateComponent() {
        super.updateComponent();

        if(getDevice() == null || getDevice().getFcm().isEmpty()) {
            if (isOnline()) {
                doRegisterFcm(true);
//                return;
            }
        }
        //prBar.setProgress(0);

//        Session.shopListChanged = 1; // list seller masih kosong semua, anggap telah terjadi perubahan pada list toko,
//        // efek untuk CtrlMessage
//
//        Session.tellShopListToRefresh = true;

        //doLoading();
    }

    @Override
    public void onBackPressed() {
        System.gc();
        finishAffinity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chaceClear();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //LocalBroadcastManager.getInstance(this).registerReceiver(broadcastGCMRegister, new IntentFilter(techsoft.com.tekansikun.controllers.service.AppPreferences.REGISTRATION_COMPLETE));

//        if(getDevice() == null || getDevice().getFcm().isEmpty()) {
//            if (isOnline()) {
//                doRegisterFcm(true);
//                return;
//            }
//        }
//
//        doRegisterFcm(false);
      //  doLoading();

    }

    private void doRegisterFcm(Boolean isNew){
        isDeviceFcmNew = isNew;
        Intent registrationIntent = new Intent(this, techsoft.com.tekansikun.controllers.service.RegistrationIntentService.class);
        startService(registrationIntent);
    }


}